PyCraft
-------

This is a collaborative project I worked on with [Grit](http://geraintwhite.co.uk){:target="_blank"}. It's a 2D Minecraft-inspired game for the terminal.

<div><img src="/~olls/readme-imgs/pycraft/game.png" alt="Climbing a hill in-game." class="no-border"></div>

Some features it sports:

- Random terrain generation with ores, hills and trees.
- Auto-save for multiple worlds.
- Splits world into chunks to improve world loading performance.
- Placing and breaking blocks.
- An inventory.

Building this was the first time I used terminal colours to their full potential, we also made a lot of progress on getting useful (game) input in the terminal, including using arrow keys! I will hopefully be adding these features to my [graphics](http://github.com/olls/graphics) module at some point.

Try out the game by downloading it from [Github](http://github.com/itsapi/pycraft), it runs best on Unix with Python3 but it is playable in windows CMD(!).