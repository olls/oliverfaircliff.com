Hire Me
-------

I am one half of [Dvbris Web Design](http://dvbris.com), we can build JavaScript, PHP or Node.js webapps and websites for you.

Contact me: [mail@oliverfaircliff.com](mailto:mail@oliverfaircliff.com){:target="_blank"} or visit [Dvbris.com](http://dvbris.com) for our portfolio.
